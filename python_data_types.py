# String data type
message = "enter some value"
# Integer data type
days = 20
# Float data type
price = 9.99
# Boolean data type with true
valid_number = True
# Boolean data type with false
exit_input = False
# List data type with integer values
list_of_days = [20, 40, 20, 100]
# List data type with string values
list_of_months = ["January", "February", "June"]
# Set data type
set_of_days= {20, 45, 100}
# Dictionary data type
days_and_unit = {"days":10, "unit":"hours"}